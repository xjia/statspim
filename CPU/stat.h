/*
 * Instruction statistic module
 *
 * @author Chenyang Wu
 */

#define STAT_TYPES 5

typedef enum stat_types {
	STAT_ALL, STAT_OTHER, STAT_LOAD, STAT_STORE,
	STAT_JUMP
} STAT_TYPE;

void stat_init ();

void stat_add_by_opcode (int);

void stat_add_by_type (STAT_TYPE tp);

int stat_get_count (STAT_TYPE tp);

char* stat_get_full_log ();

char* stat_get_short_log ();